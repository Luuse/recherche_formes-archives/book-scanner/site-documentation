# site-documentation

Une doc pour le bookscanner avec mkdocs.

Disponible temporairement sur [ce lien](https://luuse.gitlab.io/recherche_formes-archives/book-scanner/site-documentation/#03032022-levier) mais prochainement plutôt [par ici](http://archivism.luuse.io/).

## TODO
+ inclure un script pour réduire la taille des images dans le fichier `.gitlab-ci.yml`
+ Par exemple: `mogrify -resize 700x700\> img_/*.jpg`
+ lancer le script seulement si de nouvelles images sont apparues
