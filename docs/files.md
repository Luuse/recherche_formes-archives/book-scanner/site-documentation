# Fichiers

## .dxf (Drawing eXchange Format)
Plans du bookscanner, dessiné sur LibreCAD

- [vue de face](files/DXF/front.dxf)
- [vue de coté](files/DXF/leftok.dxf)

## .stl (STereo-Lithography)
modules d'assemblages imprimés en 3D

- [corner connectors](files/STL/2020_Cap.stl)
- [V-slot cap](files/STL/VSLOT_Corner_Connectors_90Angle.stl)
