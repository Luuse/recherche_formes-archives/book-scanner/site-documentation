# [pi-scan](https://github.com/Tenrec-Builders/pi-scan)

## Matériel nécessaire
+ Un Raspberry Pi 2 ou 3
![](img/059.jpg)

+ 2 appareils photos (modèles Canon PowerShot A2500 ou Canon PowerShot ELPH 160 (aka IXUS 160) ou Nikon 1 J5)
![](img/055.jpg)
![](img/058.jpg)
+ 3 cartes SD de minimum 4 Go (2 pour les appareils et un pour le Rpi)
+ un stockage en USB (clé USB ou adaptateur pour carte SD)
+ écran + clavier OU écran tactile

Pour ce scanner on a choisi un écran et un clavier et 2 appareils Canon PowerShot A2500 trouvés en seconde main.

## Installation
+ Télécharger l'image de PiScan correspondant (écran + clavier)
+ Flasher l'image sur le RPi (avec etcher par exemple)
+ Télécharger l'image de CHDK - **Canon Hack Developement Kit** (pour commander les appareils à distance)
+ La flasher sur les deux cartes SD des appareils

## Utilisation
+ Démarrer le RPi pour vérifier que ça marche (il doit afficher ça :)

![](img/052.jpg)

+ Connecter une clé USB ou un stockage externe pour enregister les photos
+ Utiliser les numéros pour naviguer

Normalement le logiciel doit reconnaître les appareils photo et pouvoir les régler et les déclencher en même temps en tapant seulement sur le clavier. On peut ensuite voir un aperçu pour vérifier que les appareils sont bien positionnés et réglés au niveau du zoom.

## Réglages

![](img/test-piscan.jpg)

Parfois il fait changer les paramètres de l'appareil photo: la date s'affichait en bas à droite de chaque photo, il faut la désactiver. Le menu de l'appareil est différent du menu de CHDK, pour aller sur le menu de l'appareil il faut appuyer sur l bouton play `🢒` avant d'appuyer sur le bouton `MENU` (pour le Canon A2500).

## Pour éteindre le RPi
+ Revenir à l'écran d'accueil
+ Attendre le message qui dit qu'on peut retirer le stockage externe ou éteindre le RPi
+ Retirer le stockage externe et/ou éteindre le Rpi (débrancher)

## (Utiliser le terminal, pas utile mais cool)
+ Utiliser le bon numéro pour revenir à la console
+ Se connecter (login: pi / password: raspberry)

## Photos de pi-scan
Avec les appareils qu'on a choisi les scans (qui sont en fait des photos) ont ces caractéristiques:

+ résolution de base = 180 dpi
+ taille de l'image de base = 4608 x 3456 px
+ 16 mégapixels
+ compression JPEG qualité 96
+ profil sRGB
+ Pas mal de données EXIF -> on pourrait les enlever pour réduire un peu la taille
