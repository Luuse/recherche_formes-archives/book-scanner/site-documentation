# Introduction 
<div class="map">
    <object type="image/svg+xml" data="img/dpp.dot.svg"></object>
</div>
[→ *English version here*](https://fablab.imal.org/#!/projects/book-scanner-by-luuse)

Les formes d'archives sont l'un des axes de recherche sur lesquels travaille Luuse. Il s'agit d'expérimenter des moyens de classer et de consulter différents types de contenus. Dans le cadre de la résidence au Fablab d'iMAL, Luuse  construit un scanner de livres. Largement inspiré par le projet [diybookscanner](https://www.diybookscanner.org/forum/viewtopic.php?p=19710) mené par Johnatan Duerig en 2016. Ce scanner de livres est la version augmentée d'un [premier objet](https://diybookscanner.org/archivist) initié par Daniel Reetz.

À partir des informations du [guide de construction](http://tenrec.builders/quill/guide/), Luuse reprend les plans et les adapte les éléments de construction en fonction des ressources du fablab. Les dessins sont réalisés sur le logiciel libreCAD. L'utilisation des ressources matérielles et techniques disponibles dans le fablab a permis la production des pièces d'assemblage sur place par impression 3D et découpe laser.

Une version anglaise est disponile sur le [site du fablab de l'iMAL](https://fablab.imal.org/#!/projects/book-scanner-by-luuse)



## 14/06/2021
- achat des premiers éléments de structure
- profilés Aluminium v-slot 2020 7x1m
- visseries diverses 
- roues x12
## 17/06/2021
- [Présentation de la découpeuse laser (Cyborg LS 1008 -K-A 100 Watt) et des imprimantes 3D (Ultimaker 2+ et Stacker S2)](https://www.imal.org/fr/fablab/equipment)
- Présentation du logiciel [Cura](https://ultimaker.com/software/ultimaker-cura)
- Dessin et production d'éléments d'assemblage
- modules de plaques et équerres d'assemblages produites en découpe laser sur PS polystyren transparent 3mm récupéré

![](img/01.jpg)
![](img/003.jpg)
![](img/004.jpg)

équerre d'angle 90° imprimées en 3D à partir du modèle trouvé sur [Thingiverse](https://www.thingiverse.com/thing:105900) 

![](img/02.jpg)

## 08/09/2021 Assemblages

Premiers essais d'assemblages

![](img/05.jpg)
![](img/010.jpg)
![](img/011.jpg)
![](img/09.jpg)
![](img/015.jpg)
![](img/014.jpg)
![](img/013.jpg)
 
Les plaques servant au module 2 (plateau assemblé sur roue) n'est pas aux bonnes mesures. Il faut redécouper ces plaques et définir les mesures exactes à partir de la structure réelle. Le plan en pdf ne constitue plus des données exactes.

## 14/10/2021 Dessin
Afin de permettre une exactitude des mesures de chaque élément, il nous paraît juste de dessiner l'objet avec un logiciel de CAO 2D paramétrique (LibreCAD). Cela nous permettra d'anticper et d'ajuster au fil des étapes de construction. 

![](img/zoom0.PNG)
![](img/zoom1.PNG)
![](img/zoom2.PNG)
![](img/zoom3.PNG)

## 30/11/2021 Module ascenseur
Les plans de face et de coté ont été dessiné sur libreCAD. les mesures sont plus juste. Cependant j'ai oublié de prendre en compte la perte de matière dûe à la lame de la scie (3mm). Les mesures ont été revues en prenant en compte cette perte. La première structure et le plateau lift sont finalisés, le module qui recevra les livre et le module en V qui accueillera les vitres sont en cours d'assemblage. 

Il est important de noter ici que nous avons eu un moment de doute intense sur les angles. Je note donc ici que découper sur une scie à onglet avec un angle positionné à 40° sur la machine correspond à 90-40. C'est le resultat d'une coupe à 40 mais d'un angle mesuré ensuite à 50° (et inversement: une coupe réalisé à 50° sur la machine donnera un angle à 40°).

![](img/026.jpg)
![](img/027.jpg)
![](img/025.jpg)
![](img/024.jpg)

## 07/12/2021 Structure 1
Découpe au laser des plaques d'assemblage pour le module en forme de V accueillant les plaques de verre.

### Spécificité technique pour le Plexiglas:
- Puissance maximale: 60%, 
- Puissance  minimale: 60%,
- Vitesse de travail: 30

![](img/028.jpg)
![](img/030.jpg)


## 17/01/2022 Module photo + lumière
Montage du module supplémentaire qui accueillera les appareils photo et les lumières.  Je suis soulagé de voir que grâce a cette structure, plus grand chose n'est bancal, ça commence à) être d'équerre, l'ascenseur glisse comme il faut etc.

![](img/031.jpg)

## 02/02/2022 Modules ailes
Découpe laser des éléments d'assemblages pour la construction des ailes réceptionnant le livre, ajustable selon l'épaisseur du dos.

![](img/038.jpg)
![](img/040.jpg)
![](img/041.jpg)
![](img/042.jpg)
![](img/044.jpg)
  

## 03/02/2022
Les panneaux sont découpés au laser dans du bois Poplar de 6 mm. 

### Spécificité technique pour le bois:
- Puissance maximale: 90%, 
- Puissance  minimale: 90%,
- Vitesse de travail: 40

![](img/047.jpg)
![](img/048.jpg)
![](img/049.jpg)
![](img/050.jpg)


## 15/02/2022 PiScan
En attendant la réception de l’ultime commande d’aluminium et de visserie, nous commençons à configurer les appareils photos Canon PowerShot A2500 en installant l'[image](tenrec.builders/pi-scan/latest/pi-scan-camera-a2500-latest.zip) sur les cartes SD  et le Raspberri Pi 2 qui éxécute le programme [PiScan](https://github.com/Tenrec-Builders/pi-scan)
 
![](img/051.jpg)
![](img/052.jpg)
![](img/059.jpg)
![](img/056.jpg)
![](img/054.jpg)

## 24/02/2022 premiers scans
La commande est arrivée, nous avons les éléments de visserie nécéssaires pour poursuivre les assemblages.

Le fait de mettre en place les bloqueurs pour l'écartement des ailes amène un problème sur la logueur de la planche, qui est bloquée par l'épaisseur du plexiglas et la surrépaisseur des vis. Nous avons donc recoupé les planches.

![](img/066.jpg)
![](img/065.jpg)

Pose des vitres, avec un fin morceau de feutre afin de les bloquer.

![](img/067.jpg)

Premiers essais de prise de vue avec les deux apareils assemblés sur la structure. Il n'y a pas encore l'éclairegae, nous avons déplacé le bookscanner sous un néon du fablab, et la vitre n'a pas été nettoyée

![](img/068.jpg)
![](img/069.jpg)

Premier résultat d'une prise de vue avec Piscan: la definition est ok, le focus aussi, mais la position des apareils mérite d'être mieux réglée.

![](img/070.jpg)
![](img/071.jpg)

Premiers essais de traitement des photos par Thomas.

![](img/072.png)
![](img/073.png)

## 03/03/2022 levier

Nous avons fixé à la structure le levier qui permettra de faire remonter le module qui accueille le livre et le plaque contre le verre.

![](img/076.jpg)

Une corde est attachée au module et à la barre, sa longueur optimale reste encore à définir.

![](img/077.jpg)
![](img/075.jpg)

## 14/03/2022 invitation à l'erg

Alexia de Visscher a invité Luuse dans le cadre de son cours de [culture numérique à l'Erg](http://culturesnumeriques.erg.be/).
Nous avons été amené à parler du bookscanner et des autres projets qui entrent dans l'axe de recherche archivism.luuse.io.
La re-rencontre avec les éttudiant·es responsables du [rideau de perle](https://wiki.erg.be/w/Rideau_de_perles) nous a amené à échanger sur les enjeux de nos projets respectifs.
Le rideau de perle a construit la première version du bookscanner, et a présenté son interface de consultation/dépot/téléchargement de document en local avec Pandora, ainsi que la ressourcerie bibliographique via [Zotero](https://www.zotero.org/groups/2622649/rideau_de_perles_-_rdp/library).
Beaucoup d'initiatives similaire à celles que Luuse met en place.

## 23/03/2022 Réajustements

Après avoir revu le bookscanner du rideau de perle lors de notre intervention à l'erg, nous avons décidé de réduire la hauteur de notre bookscanner en enlevant 10cm aux 4 colonnes verticales de la structure principale. On imagine que ca va augmenter la stabilité de l'objet.
Nous avons aussi raccourci les deux barres supportant chacune les deux colonnes. Ces barres font maintenant la même profondeur que le reste des barres structurant les autres modules (44cm).

![](img/073.jpg)

Le réajustement des distance permettent (peut-ëtre?) un meilleur coulissement du module ascenseur.

![](img/072.jpg)

Les cordes, malgré leur qualité moyenne, attachées au levier permettent maintenant de faire monter et descendre le module ascenseur!
 
![](img/074.gif)


## 05/04/2022 Module lumière I

Quelques réflexions et décisions prises pour le module lumière, dernier élément de structure à intégrer au Bookscanner. Après être passés par différentes étapes de choix de matériaux, le fablab nous a fourni une chute de rideau occultant. Alice, Félou et Thomas ont imaginé le principe d'une "housse-toit".
Ici en prototype avec une housse de matelas :)

![](img/078.jpg)

Alice a dessiné les plans de la structure permettant d'attacher par la suite les bandes de LED et permettre un tombé de la housse en triangle.

![](img/079.jpg)


## 06/04/2022 Module lumière II

Mise en place de la structure supplémentaire, accrochage puis placement provisoire du fameux textile occultant. 
![](img/080.jpg)
![](img/081.jpg)
![](img/082.jpg)

Quelques essais de numérisation ont été fait ce jour là pour avoir de quoi poursuivre le travail sur scaNickel.

<video width="320" height="240" controls>
  <source src="img/capture-generating.mp4" type="video/mp4">
</video>

![](img/083.jpg)


En l'état, et une fois que Pi-scan est reglé, la numérisation d'une double page de livre met environ on 10 secondes en comptant le mouvement de l'ascenseur.
On peut alors estimer la durée de numérisation intégrale d'un livre.

### Calcul pour un livre de 250 pages:  
- 250/2 = 125 doubles pages
- nombre de secondes par doubles pages= 125x10 = 1250 secondes
- nombres de minutes = 1250/60 = 20

![](img/time.gif)



